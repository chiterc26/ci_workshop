#!/bin/bash

apt update


apt install apache2 nginx mysql-server -y
apt install php libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y


systemctl restart apache2


mkdir /var/www/wordpress && chown -R $USER:$USER /var/www/wordpress


echo "
<VirtualHost *:80>
    ServerName pripevpesni.fun
    ServerAlias www.pripevpesni.fun
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/wordpress
    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
     <Directory /var/www/wordpress/>
    AllowOverride All
</Directory>
</VirtualHost>
" >> /etc/apache2/sites-available/wordpress.conf


a2ensite wordpress

a2dissite 000-default

apache2ctl configtest


systemctl reload apache2


echo "<?php
phpinfo();
?>" > /var/www/wordpress/info.php


mysql <<EOF
CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER 'wordpressuser'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL ON wordpress.* TO 'wordpressuser'@'%';
FLUSH PRIVILEGES;
EXIT;
EOF


a2enmod rewrite


apache2ctl configtest


systemctl restart apache2


cd /tmp
curl -O https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
touch /tmp/wordpress/.htaccess
cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php


sed -i "s/define( 'DB_NAME', 'database_name_here' );/define( 'DB_NAME', 'wordpress' );/g" /tmp/wordpress/wp-config.php
sed -i "s/define( 'DB_USER', 'username_here' );/define( 'DB_USER', 'wordpressuser' );/g" /tmp/wordpress/wp-config.php
sed -i "s/define( 'DB_PASSWORD', 'password_here' );/define( 'DB_PASSWORD', 'password' );/g" /tmp/wordpress/wp-config.php

mkdir /tmp/wordpress/wp-content/upgrade
sudo cp -a /tmp/wordpress/. /var/www/wordpress
sudo chown -R www-data:www-data /var/www/wordpress
sudo find /var/www/wordpress/ -type d -exec chmod 750 {} \;
sudo find /var/www/wordpress/ -type f -exec chmod 640 {} \;


